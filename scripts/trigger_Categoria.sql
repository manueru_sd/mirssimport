create OR alter trigger agregarCategoriaPorDefecto on Posts 
for insert 
 as 
	begin 
		if ((select id_categoria from posts where ID_Categoria like null) IS null) 
		begin 
			update Posts set id_categoria = 1 where ID = (select top 1 ID from inserted) 
		end 
	end 