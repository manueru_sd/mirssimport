DROP TABLE IF EXISTS Usuarios 
CREATE TABLE Usuarios (
	ID int IDENTITY(1,1) PRIMARY KEY,
	Nombre Varchar(100) not null
); 

DROP TABLE IF EXISTS Posts 
CREATE TABLE Posts (
	ID int IDENTITY(1,1) PRIMARY KEY,
	Fecha DATETIME not null,
	Titulo Varchar(100) not null,
	Cuerpo varchar(max) not null,
	ID_Autor int not null,
	ID_Categoria int
); 

DROP TABLE IF EXISTS Categorias 
CREATE TABLE Categorias (
	ID int IDENTITY(1,1) PRIMARY KEY,
	Nombre Varchar(100) not null
); 

ALTER TABLE Posts ADD CONSTRAINT FK_Posts_Usuarios FOREIGN KEY (ID_Autor) REFERENCES Usuarios(ID); 

ALTER TABLE Posts ADD CONSTRAINT FK_Posts_Categorias FOREIGN KEY (ID_Categoria) REFERENCES Categorias(ID); 
