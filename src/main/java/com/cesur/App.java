package com.cesur;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.io.BufferedReader;
//import java.io.FileNotFoundException;
import java.io.FileReader;
//import java.io.IOException;


public class App 
{
    private static String serverDB="localhost";
    private static String portDB="1533";
    private static String DBname="Blog";
    private static String userDB="sa";
    private static String passwordDB="12345Ab##";
    /** 
     * @param args
     */
    public static void main( String[] args )
    {
        
        int respuesta=10;
do {
        System.out.println("Selecciona una opción: \n "
        +"0: Crear la base de datos. \n "
        +"1: Importar desde un RSS los contenidos a la base de datos \n "
        +"2: Ver ultimos 10 post (se muestra el ID del post, la fecha y titulo) \n "
        +"3: Ver todos los post \n "
        +"4: Ver en detalle un solo post  \n "
        +"5: Buscar en el titulo o el contenido una palabra  \n "
        +"6: Filtrar por categoria \n "
        +"10: Salir.");
        
        respuesta = solicitarYValidarOpcionMenu("Elija una opcion: ", 0, 10);

        switch (respuesta) {
            case 0:
                CrearBBDD();
                crearTrigger();
                break;
            case 1:
                importarRSS();
                break;
            case 2:
                verUltimosNPost(10);
                break;
            case 3:
                verTodosLosPost();
                break;
            case 4:
                verPostEnDetalle();
                break;
            case 5:
                buscarPorTituloOContenido();
                break;
            case 6:
                filtrarPorCategoria();
                break;
        }
    } while (respuesta != 10); 

    }

    private static void CrearBBDD()
    {
        bbdd b = new bbdd(serverDB,portDB,"master",userDB,passwordDB);
        
        String i= b.leerBBDDUnDato("SELECT count(1) FROM sys.databases  where name='"+DBname+"' ");
     
        if (i.equals("0")){
            b.modificarBBDD("CREATE DATABASE " + DBname); 
            b=new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
            String query = LeerScriptBBDD("scripts/CrearBlog.sql");
            b.modificarBBDD(query);  
        }
       
        System.out.println("Se ha creado la base de datos.");
    }

    private static void crearTrigger() {
        
        bbdd b=new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        String query = LeerScriptBBDD("scripts/trigger_Categoria.sql");
        b.modificarBBDD(query);  
        
        System.out.println("Se ha creado el trigger.");
    }

    private static String LeerScriptBBDD(String archivo){
       String ret = "";
        try {
            String cadena;
            FileReader f = new FileReader(archivo);
            BufferedReader b = new BufferedReader(f);
            while((cadena = b.readLine())!=null) {
                ret = ret + cadena;
                //System.out.println(ret);
            }
            b.close();
        } catch (Exception e) {
        }
       return ret;

    }

    private static void importarRSS() {
        
        //solicitar url

        XmlParse x = new XmlParse();
        
        
        String url = "https://lenguajedemarcasybbdd.wordpress.com/feed/";
        
        //leer fechas posts
        System.out.println("Extrayendo fechas...");
        List<String> fechas = x.leer("rss/channel/item/pubDate/text()", url);
        System.out.println("...fechas extraidas");

        //leer titulos posts
        System.out.println("Extrayendo titulos...");
        List<String> titulos = x.leer("rss/channel/item/title/text()", url);
        
        System.out.println("...titulos extraidos");

        //leer cuerpos posts
        System.out.println("Extrayendo contenido de los posts...");
        List<String> cuerpos = x.leer("rss/channel/item/*[name()='content:encoded']/text()", url);

        System.out.println("...contenidos extraidos");

        //leer autores --> el nombre es para la tabla usuarios
        System.out.println("Extrayendo autores...");
        List<String> autores = x.leer("rss/channel/item/*[name()='dc:creator']/text()", url);
        
        System.out.println("...autores extraidos");

        //leer categorias --> nombre tabla categorias
        System.out.println("Extrayendo categorias...");
        List<String> categorias = x.leer("rss/channel/item/category/text()", url);
        
        System.out.println("...categorias extraidas");

        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);

        //insertar autores/usuarios
            //para eliminar los autores duplicados en la lista
        Set<String> s = new LinkedHashSet<>(autores);
        List<String> autores2 = new ArrayList<String>(s);

        for (int j = 0; j < autores2.size(); j++) {
            b.modificarBBDD("insert into usuarios (nombre) values ('"+autores2.get(j)+"')"); 
        }
        
        //insertar categorias
            //para eliminar las categorias duplicadas en la lista
        s = new LinkedHashSet<>(categorias);
        List<String> categorias2 = new ArrayList<String>(s);
        
        for (int j = 0; j < categorias2.size(); j++) {
            b.modificarBBDD("insert into categorias (nombre) values ('"+categorias2.get(j)+"')");
        }
            
        
        //insertar posts
        for (int j = 0; j < fechas.size(); j++) {
            //extraer el los id correspondientes del autor y de la categoria
            String id_autor = b.leerBBDDUnDato("select top 1 id from usuarios where nombre = '"+autores.get(j)+"' ");
            String id_categoria = b.leerBBDDUnDato("select top 1 id from categorias where nombre = '"+categorias.get(j)+"' ");

            //adaptar el formato de la fecha de "Thu, 08 Apr 2021 20:06:07 +0000" a "YYYY-MM-DD HH:MI:SS"
            String[] partesFecha = fechas.get(j).split("[ :]");
            String anno = partesFecha[3];
            String mes = numeroMes(partesFecha[2]);
            String dia = partesFecha[1];
            String hora = partesFecha[4];
            String minuto = partesFecha[5];
            String segundo = partesFecha[6];
            String amOpm;
            if (Integer.parseInt(hora) >= 12) {
                amOpm = "PM";
            } else {
                amOpm = "AM";
            }
            //'20120618 10:34:09 AM'
            String fecha = anno + mes + dia +" "+ hora +":"+ minuto +":"+ segundo + " " + amOpm;

            b.modificarBBDD("insert into posts (fecha, titulo, Cuerpo, ID_Autor, ID_Categoria) values ('"+fecha+"', '"+titulos.get(j)+"', '"+cuerpos.get(j)+"', '"+id_autor+"', '"+id_categoria+"')");
        }

        System.out.println("RSS insertado en la base de datos");

    }

    public static String numeroMes (String mesLetras) {
        String mes="";

        switch (mesLetras) {
            case "Jan":
                mes = "01";
                break;
            case "Feb":
                mes = "02";
                break;
            case "Mar":
                mes = "03";
                break;
            case "Apr":
                mes = "04";
                break;
            case "May":
                mes = "05";
                break;
            case "Jun":
                mes = "06";
                break;
            case "Jul":
                mes = "07";
                break;
            case "Aug":
                mes = "08";
                break;
            case "Sep":
                mes = "09";
                break;
            case "Oct":
                mes = "10";
                break;
            case "Nov":
                mes = "11";
                break;
            case "Dec":
                mes = "12";
                break;
        }
        return mes;
    }


    private static void verUltimosNPost(int numPosts) {
       
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        String query = "SELECT top "+numPosts+" ID, fecha, titulo FROM Posts";
        b.leerBBDD(query, 3);  
  
    }

    private static void verTodosLosPost() {
       
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        String query = "SELECT ID, fecha, titulo FROM Posts";
        b.leerBBDD(query, 3);  
  
    }

    private static void verPostEnDetalle() {
       
        Scanner teclado = new Scanner(System.in);
        System.out.print("Introduce el ID: ");
        int id = teclado.nextInt();
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);

        //String query1 = "SELECT titulo, u.nombre as autor, cuerpo, fecha, c.nombre FROM Posts as p inner join Categorias as c on p.ID_Categoria = c.ID inner join usuarios as u on p.ID_Autor = u.ID WHERE p.ID = "+id+" ";

        //titulo
        System.out.print("Titulo: ");
        String query = "SELECT titulo FROM Posts WHERE ID = "+id+" ";
        b.leerBBDD(query, 1); 

        //autor
        System.out.print("Autor: ");
        query = "SELECT u.nombre as autor FROM Posts as p inner join usuarios as u on p.ID_Autor = u.ID WHERE p.ID = "+id+" ";
        b.leerBBDD(query, 1);

        //cuerpo
        System.out.print("Cuerpo: ");
        query = "SELECT cuerpo FROM Posts WHERE ID = "+id+" ";
        b.leerBBDD(query, 1);

        //fecha
        System.out.print("Fecha: ");
        query = "SELECT fecha FROM Posts WHERE ID = "+id+" ";
        b.leerBBDD(query, 1);

        //categoria
        System.out.print("Categoria: ");
        query = "SELECT c.nombre FROM Posts as p inner join Categorias as c on p.ID_Categoria = c.ID WHERE p.ID = "+id+" ";
        b.leerBBDD(query, 1);
         
  
    }

    private static void buscarPorTituloOContenido() {

        Scanner teclado = new Scanner(System.in);
        System.out.print("Introduce la palabra: ");
        String palabra = teclado.nextLine();
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
        
        String query = "SELECT titulo from Posts WHERE titulo LIKE '%"+palabra+"%'";
        ArrayList<String> resultadoTitulos = b.leerBBDDUnDatoALista(query, "titulo");

        query = "SELECT cuerpo from Posts WHERE cuerpo LIKE '%"+palabra+"%'";
        ArrayList<String> resultadoContenidos = b.leerBBDDUnDatoALista(query, "cuerpo");

        System.out.println("Titulos que contienen la palabra: ");
        for (int i = 0; i < resultadoTitulos.size(); i++) {
            System.out.println(resultadoTitulos.get(i));
            System.out.println("--------------------------------------------");
        }

        System.out.println("Contenidos que contienen la palabra: ");
        for (int i = 0; i < resultadoContenidos.size(); i++) {
            System.out.println(resultadoContenidos.get(i));
            System.out.println("--------------------------------------------");
        }
        
    }

    private static void filtrarPorCategoria() {
        //mostrar las categorias
        Scanner teclado = new Scanner(System.in);
        
        bbdd b = new bbdd(serverDB,portDB,DBname,userDB,passwordDB);
       
        System.out.println("Estas son las categorias disponibles:");
        String query = "SELECT ID, nombre from categorias";
        b.leerBBDD(query, 2);

        //elegir una
        System.out.print("Elige el id de la categoria: ");
        int categoria = teclado.nextInt();

        //mostrar los posts de esa categoria
        query = "SELECT ID, fecha, titulo, id_categoria FROM Posts where id_categoria = '"+categoria+"'";
        b.leerBBDD(query, 3); 

        System.out.println();
    }

    //validador del menu	
	public static int solicitarYValidarOpcionMenu(String texto, int min, int max) {
		
		int opcion=0;
		Scanner teclado = new Scanner(System.in);
		
		do {
			try {
				System.out.print(texto);
				opcion = teclado.nextInt();
			} catch (InputMismatchException ime) {
				System.out.println("Solo puedes insertar numeros.");
				teclado.next();
			}
		} while (opcion < min || opcion > max);
		
		//teclado.close();
		
		return opcion;
	}

}
